Ссылка на репозитории с кодом для каждого приложения:
- DiceRoll: https://gitlab.com/sharuevapolina/android-building-app-ui/-/tree/master
- ArtApp: https://gitlab.com/sharuevapolina/android-building-app-ui-artspace
- TipCalculator: https://gitlab.com/sharuevapolina/android-building-app-ui-tiptime
- Lemonade: https://gitlab.com/sharuevapolina/building-app-ui/-/tree/master
